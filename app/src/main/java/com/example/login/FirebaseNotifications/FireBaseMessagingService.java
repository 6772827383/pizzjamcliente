package com.example.login.FirebaseNotifications;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.example.login.MainActivity;
import com.example.login.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

public class FireBaseMessagingService extends FirebaseMessagingService {


    public static final String TAG="Notificacion";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String From=remoteMessage.getFrom();

        Log.d("", "onMessageReceived: "+From);

        if (remoteMessage.getNotification()!=null){
            mostrarNotificacion(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody());
            Log.d(TAG, "Notificacion:"+ remoteMessage.getNotification().getBody());
        }
        else{
            if (remoteMessage.getData() != null) {
                mostrarNotificacion(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"));
            }
        }
    }

    private void mostrarNotificacion(String title, String body) {

        String Nombre="Chales";
        //new Alarma.execute(Nombre.toString());
        Log.d("NOTIFICACIONFB", "Reciida");
        Random rnd = new Random();
        Random rnd2 = new Random();
        int r = rnd.nextInt();
        int r2 = rnd2.nextInt();
        int id = (r * r2)+r;
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "102356";
        Intent intent = new Intent(this, MainActivity.class); //Set the activity
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("fragment","order_Fragment");
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_MAX);


            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);


            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);

            notificationBuilder.setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                    .setTicker("ApptojoBusiness pr the name of the app")
                    .setContentIntent(pendingIntent)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setContentInfo(body).setColor(getBaseContext().getResources().getColor(R.color.colorPrimary))
                    .setSound(defaultSoundUri);

            notificationManager.notify(id, notificationBuilder.build());
        }else{

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_menu_camera)
                    .setContentTitle(title)
                    .setContentText(body).setColor(getBaseContext().getResources().getColor(R.color.colorPrimary))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .setContentIntent(pendingIntent);
            NotificationManager notificationManager2 = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager2.notify(id, notificationBuilder.build());
        }

    }

}
