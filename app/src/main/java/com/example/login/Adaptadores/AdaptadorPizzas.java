package com.example.login.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.login.Modelos.Base;
import com.example.login.Modelos.PizzasPo;
import com.example.login.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class AdaptadorPizzas extends RecyclerView.Adapter<AdaptadorPizzas.ViewHolder> implements View.OnClickListener{
    Context mContext;
    ArrayList<Base> PizzasPopulares;
    private View.OnClickListener listener;

    public AdaptadorPizzas(Context mContext, ArrayList<Base> pizzasPopulares) {
        this.mContext = mContext;
        PizzasPopulares = pizzasPopulares;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext)
                .inflate(R.layout.item_pizza,parent,false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdaptadorPizzas.ViewHolder holder, int position) {
        Glide.with(mContext)
                .load(PizzasPopulares.get(position).getImagen_B())
                .error(R.drawable.pizzafondo)
                .into(holder.ImagenPizza);
        holder.nombrePizza.setText(PizzasPopulares.get(position).getNombre());
        int tamañoMedio=PizzasPopulares.get(position).getTamaños().size()/2;
        holder.PizzaPrecio.setText(PizzasPopulares.get(position).getTamaños().get(tamañoMedio).getPrecio());
        holder.Pizzatam.setText(PizzasPopulares.get(position).getTamaños().get(tamañoMedio).getDiametro());
    }

    @Override
    public int getItemCount() {
        return PizzasPopulares.size();
    }


    public void setOnClickListener(View.OnClickListener listener){
        this.listener=listener;
    }
    @Override
    public void onClick(View v) {
        if (listener!=null){
            listener.onClick(v);
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ImagenPizza;
        TextView nombrePizza, Pizzatam, PizzaPrecio;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ImagenPizza=itemView.findViewById(R.id.ImagenPizza);
            nombrePizza=itemView.findViewById(R.id.nombrePizza);
            Pizzatam=itemView.findViewById(R.id.Pizzatam);
            PizzaPrecio=itemView.findViewById(R.id.PizzaPrecio);
        }
    }
}
