package com.example.login.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.login.Item_tama;
import com.example.login.R;

import java.util.ArrayList;

public class AdaptadorItemTama extends ArrayAdapter<Item_tama> {

    public AdaptadorItemTama (Context context, ArrayList<Item_tama> TamaLista){
        super(context,0,TamaLista);

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position,convertView,parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position,convertView,parent);
    }

    private View initView(int posicion, View convertView, ViewGroup parent)
    {
        if (convertView==null){
            convertView= LayoutInflater.from(getContext()).inflate(
                     R.layout.item_tamano,parent,false
             );
        }
        ImageView imageView = convertView.findViewById(R.id.img_tama);
        TextView textvView = convertView.findViewById(R.id.tamaño);

        Item_tama itemActual = getItem(posicion);

        if(itemActual!=null){
            imageView.setImageResource(itemActual.getImag());
            textvView.setText(itemActual.getNombre()+" ("+itemActual.getTamaño()+")");
        }
        return convertView;
    }
}
