package com.example.login;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.login.Adaptadores.AdaptadorItemTama;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CardPizzas extends AppCompatActivity {
    public ArrayList<Item_tama> mlista;
    public AdaptadorItemTama adaptadorItemTama;
    ArrayList<String> tokens;
    ArrayList<String> nombres;
    ArrayList<String> tamaños;
    ArrayList<String> precios;
    ImageView ImagenPizzaC;
    Button comprar;
    TextView TitlePizza,Descripcion,PrecioCard;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_pizzas);
        String Imagen=getIntent().getStringExtra("Imagen");
        String Nombre=getIntent().getStringExtra("Nombre");
        final String Precio=getIntent().getStringExtra("Precio");
        final String Token=getIntent().getStringExtra("token").toString();

        ImagenPizzaC=findViewById(R.id.ImagenPizzaC);
        TitlePizza=findViewById(R.id.TitlePizza);
        Descripcion=findViewById(R.id.Descripcion);
        PrecioCard=findViewById(R.id.PrecioCard);
        comprar=findViewById(R.id.comprar);
       nombres=new ArrayList<>();
        tamaños=new ArrayList<>();
        precios=new ArrayList<>();
        Log.d("algo", "antes del desmadre");
        nombres.addAll(getIntent().getStringArrayListExtra("nombres"));
        Log.d("algo", "ufff 1");
        tamaños.addAll(getIntent().getStringArrayListExtra("tamaños"));
        Log.d("algo", "ufffffffffffffffff 2");
        precios.addAll(getIntent().getStringArrayListExtra("Precios"));
        Log.d("algo", "la tormenta ah pasado");
        Glide.with(getApplicationContext())
                .load(Imagen)
                .error(R.drawable.pizzafondo)
        .into(ImagenPizzaC);
        TitlePizza.setText(Nombre);
        PrecioCard=findViewById(R.id.PrecioCard);

        comprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendNotification(Token,"","");
            }
        });

        initlist();
        Spinner spinnerTama = findViewById(R.id.SpinTama);

        adaptadorItemTama = new AdaptadorItemTama(this, mlista);
        spinnerTama.setAdapter(adaptadorItemTama);

        spinnerTama.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Item_tama clickedItem = (Item_tama) parent.getItemAtPosition(position);
                String clickedName = clickedItem.getNombre();

                Toast.makeText(CardPizzas.this, clickedName.toString()+"me toucheaste", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initlist() {
        mlista = new ArrayList<>();
        for (int i = 0; i < precios.size(); i++) {
            mlista.add(new Item_tama(nombres.get(i),precios.get(i),tamaños.get(i), R.drawable.ic_circulo));
        }

    }

    public void sendNotification(String regToken, final String title, final String message) {
        //   Log.d(TAG, "sendNotification: TOKEN "+regToken);
        final String regToken2 = regToken;
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        final String LEGACY_SERVER_KEY = "AAAAMB5gl_Q:APA91bENwskrjMi18K9kbDjyp2kfgBi5dM8ziI63Zijd8P0ip4Iyb1cQRKMo8afg6Hu1J680sgCFft0NNcs5wNCE5lwsC3NeXBHgfGuUd1LAE8w9kgv7Ak28ueFlw7n3xSw9AwVvfgfe";
        final MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");

        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json=new JSONObject();
                    JSONObject dataJson=new JSONObject();
                    dataJson.put("body","Se te ah solicitado una pizza");
                    dataJson.put("title","PizzJamApp");
                    dataJson.put("sound","default");
                    dataJson.put("priority","alta");

                    json.put("data",dataJson);
                    Log.d("TOKEN_DEL_NEGOCIO","El token es"+regToken2);
                    json.put("to",regToken2);
                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization","key="+ LEGACY_SERVER_KEY)
                            .url("https://fcm.googleapis.com/fcm/send")
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                }catch (Exception e){
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();



    }
}
