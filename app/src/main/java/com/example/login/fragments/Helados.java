package com.example.login.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.login.Adaptadores.AdaptadorPizzas;
import com.example.login.CardPizzas;
import com.example.login.Contenido;
import com.example.login.Modelos.Base;
import com.example.login.Modelos.PizzasPo;
import com.example.login.Modelos.tamaños;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import com.example.login.Adaptador;
import com.example.login.R;
import com.example.login.item;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageClickListener;
import com.synnapps.carouselview.ImageListener;


public class Helados extends Fragment {


    CarouselView cvCar;
    RecyclerView RvPiPo,RvPiPe;
    ArrayList<item> Lista;
    FirebaseDatabase dbr;
    DatabaseReference dbrPiPo;
    DatabaseReference dbrnegocio;
    Context mContext;


    //listas de los recyclers
    ArrayList<Base> PizzasT;
    ArrayList<Base> PizzasPopulares;
    ArrayList<Base> PizzaMasPedidas;
    ArrayList<String> tokens;

    //adaptadores
    AdaptadorPizzas adPi,adPe;

    int [] fondos={R.drawable.pizzafondo,R.drawable.ic_pizza,R.drawable.ic_pepperoni};
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext=getContext();
        dbr = FirebaseDatabase.getInstance();
        dbrPiPo= dbr.getReference().child("Pizzas");
        dbrnegocio=dbr.getReference().child("Negocios");
        tokens=new ArrayList();
        //final String Token=getIntent().getStringExtra("token").toString();
        PizzasT=new ArrayList<>();
        PizzasPopulares=new ArrayList<>();
        PizzaMasPedidas=new ArrayList<>();
        View vista=inflater.inflate(R.layout.fragment_helados, container, false);
        cvCar=vista.findViewById(R.id.cvCar);
        cvCar.setPageCount(fondos.length);
        cvCar.setImageListener(imagelistener);

        cvCar.setImageClickListener(new ImageClickListener() {
            @Override
            public void onClick(int position) {
                Toast.makeText(mContext, "no le puse nada a eso :'v", Toast.LENGTH_SHORT).show();
            }
        });
        RvPiPo=vista.findViewById(R.id.RvPiPo);
        RvPiPe=vista.findViewById(R.id.RvPiPe);
dbrnegocio.addValueEventListener(new ValueEventListener() {
    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        if (dataSnapshot!=null){
            for (DataSnapshot Negocios:dataSnapshot.getChildren()) {
                tokens.add(Negocios.child("tokens").getValue().toString());
            }
        }
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }
});
        dbrPiPo.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                PizzasT.clear();
                if (dataSnapshot!=null){
                    for (DataSnapshot Negocios:dataSnapshot.getChildren()) {
                        for (DataSnapshot Pizzas : Negocios.getChildren()) {
                            Toast.makeText(mContext, "Pizzas"+Pizzas.getKey(), Toast.LENGTH_SHORT).show();
                                Base b=new Base();
                                b.setNombre(Pizzas.child("nombre").getValue().toString());
                                b.setImagen_B(Pizzas.child("Imagen_B").getValue().toString());
                                b.setPizzasPedidas(Pizzas.child("PizzasPedidas").getValue().toString());
                                b.setPizzaVistas(Pizzas.child("PizzaVistas").getValue().toString());
                                ArrayList<String> CIng=new ArrayList<>();
                                for (DataSnapshot Carnes:Pizzas.child("Carnes").getChildren()){
                                    CIng.add(Carnes.getValue().toString());
                                }
                                b.setCarnes(CIng);
                                ArrayList<String> QIng=new ArrayList<>();
                                for (DataSnapshot Quesos:Pizzas.child("Quesos").getChildren()){
                                    QIng.add(Quesos.getValue().toString());
                                }
                                b.setQuesos(QIng);
                                ArrayList<String> VIng=new ArrayList<>();
                                for (DataSnapshot Verduras:Pizzas.child("Frutas_Verduras_y_Hongos").getChildren()){
                                    VIng.add(Verduras.getValue().toString());
                                }
                                b.setFrutas_Verduras_y_Hongos(VIng);
                                ArrayList<tamaños> TS=new ArrayList<>();
                                for (DataSnapshot Tam:Pizzas.child("Tamaños").getChildren()){
                                    TS.add(Tam.getValue(tamaños.class));
                                }
                                b.setTamaños(TS);
                                    PizzasT.add(b);
                            }
                        }
                    }
                OrdenarPizzasPopulares();
                adPi.notifyDataSetChanged();
                adPe.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        adPi=new AdaptadorPizzas(getContext(),PizzasPopulares);
        RvPiPo.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false));
        adPi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i=RvPiPo.getChildAdapterPosition(v);
                Intent intent=new Intent(mContext, CardPizzas.class);
                intent.putExtra("Nombre",PizzasT.get(i).getNombre());
                intent.putExtra("Imagen",PizzasT.get(i).getImagen_B());
                intent.putExtra("token",tokens.get(i).toString());
                intent.putExtra("Precio",PizzasT.get(i).getTamaños().get(0).getPrecio());
                ArrayList<String>nombres=new ArrayList<>();
                for (int j = 0; j < PizzasT.get(i).getTamaños().size(); j++) {
                    nombres.add(PizzasT.get(i).getTamaños().get(j).getNombre());
                }
                intent.putStringArrayListExtra("nombres",nombres);
                ArrayList<String>tamaños=new ArrayList<>();
                for (int j = 0; j < PizzasT.get(i).getTamaños().size(); j++) {
                    tamaños.add(PizzasT.get(i).getTamaños().get(j).getDiametro());
                }
                intent.putStringArrayListExtra("tamaños",tamaños);
                ArrayList<String>Precios=new ArrayList<>();
                for (int j = 0; j < PizzasT.get(i).getTamaños().size(); j++) {
                    Precios.add(PizzasT.get(i).getTamaños().get(j).getPrecio());
                }
                intent.putStringArrayListExtra("Precios",Precios);

                startActivity(intent);
            }
        });
        RvPiPo.setAdapter(adPi);
        adPe=new AdaptadorPizzas(getContext(),PizzaMasPedidas);
        RvPiPe.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false));
        adPe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i=RvPiPe.getChildAdapterPosition(v);
                Intent intent=new Intent(mContext, CardPizzas.class);
                intent.putExtra("Nombre",PizzasT.get(i).getNombre());
                intent.putExtra("Imagen",PizzasT.get(i).getImagen_B());
                intent.putExtra("token",tokens.get(i).toString());
                intent.putExtra("Precio",PizzasT.get(i).getTamaños().get(0).getPrecio());
                ArrayList<String>nombres=new ArrayList<>();
                for (int j = 0; j < PizzasT.get(i).getTamaños().size(); j++) {
                    nombres.add(PizzasT.get(i).getTamaños().get(j).getNombre());
                }
                intent.putStringArrayListExtra("nombres",nombres);
                ArrayList<String>tamaños=new ArrayList<>();
                for (int j = 0; j < PizzasT.get(i).getTamaños().size(); j++) {
                    tamaños.add(PizzasT.get(i).getTamaños().get(j).getDiametro());
                }
                intent.putStringArrayListExtra("tamaños",tamaños);
                ArrayList<String>Precios=new ArrayList<>();
                for (int j = 0; j < PizzasT.get(i).getTamaños().size(); j++) {
                    Precios.add(PizzasT.get(i).getTamaños().get(j).getPrecio());
                }
                intent.putStringArrayListExtra("Precios",Precios);

                startActivity(intent);
            }
        });
        RvPiPe.setAdapter(adPe);
        Toast.makeText(mContext, PizzasT.size()+"Pizzas"+PizzasPopulares.size(), Toast.LENGTH_SHORT).show();
        return vista;
    }

    private void OrdenarPizzasPopulares() {
        PizzasPopulares.clear();
        PizzaMasPedidas.clear();
        for (int i = 0; i <PizzasT.size() ; i++) {
            for (int j = i+1; j <PizzasT.size() ; j++) {
                int vistas1=Integer.parseInt(PizzasT.get(i).getPizzaVistas());
                int vistas2=Integer.parseInt(PizzasT.get(j).getPizzaVistas());

                if (vistas1<vistas2){
                    Base p=new Base();
                    p=PizzasT.get(i);
                    PizzasT.set(i,PizzasT.get(j));
                    PizzasT.set(j,p);
                }
            }
        }

        for (int i = 0; i <PizzasT.size() || i<4; i++) {
            PizzasPopulares.add(PizzasT.get(i));
        }

        for (int i = 0; i <PizzasT.size() ; i++) {
            for (int j = i+1; j <PizzasT.size() ; j++) {
                int vistas1=Integer.parseInt(PizzasT.get(i).getPizzasPedidas());
                int vistas2=Integer.parseInt(PizzasT.get(j).getPizzasPedidas());

                if (vistas1<vistas2){
                    Base p=new Base();
                    p=PizzasT.get(i);
                    PizzasT.set(i,PizzasT.get(j));
                    PizzasT.set(j,p);
                }
            }
        }

        for (int i = 0; i <PizzasT.size() || i<4; i++) {
            PizzaMasPedidas.add(PizzasT.get(i));
        }

    }

    ImageListener imagelistener=new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(fondos[position]);
        }
    };
}