package com.example.login.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.login.Adaptador;
import com.example.login.Adaptadores.AdaptadorNegocios;
import com.example.login.CardNegocios;
import com.example.login.Contenido;
import com.example.login.Modelos.Negocio;
import com.example.login.R;
import com.example.login.item;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;

public class Comida extends Fragment {

    RecyclerView RvNegocio;
    ArrayList<Negocio> AlNegocios;
    ArrayList <Float>AlCalif;
    ArrayList<String> tokens;
    DatabaseReference dbr;
    Context mContext;


    AdaptadorNegocios AdNeg;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext=getContext();
        View vista=inflater.inflate(R.layout.fragment_comida, container, false);
        RvNegocio=vista.findViewById(R.id.RvNegocio);
        dbr= FirebaseDatabase.getInstance().getReference().child("Negocios");
        AlNegocios=new ArrayList<>();
        AlCalif=new ArrayList<>();
        tokens=new ArrayList<>();
        AdNeg=new AdaptadorNegocios(AlNegocios, AlCalif, mContext, -1);
        RvNegocio.setLayoutManager(new GridLayoutManager(mContext,3));
        AdNeg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext, CardNegocios.class);
                intent.putExtra("UID",AlNegocios.get(RvNegocio.getChildAdapterPosition(v)).getUid_Negocio());
                intent.putExtra("token", tokens.get(RvNegocio.getChildAdapterPosition(v)));
                intent.putExtra("Imagen",AlNegocios.get(RvNegocio.getChildAdapterPosition(v)).getImagen_N());
                intent.putExtra("Nombre",AlNegocios.get(RvNegocio.getChildAdapterPosition(v)).getNombre_Negocio());
                intent.putExtra("Direccion",AlNegocios.get(RvNegocio.getChildAdapterPosition(v)).getCalle_N()+" #"+AlNegocios.get(RvNegocio.getChildAdapterPosition(v)).getNumeroD_N()+","+AlNegocios.get(RvNegocio.getChildAdapterPosition(v)).getColonia_N());
                startActivity(intent);
            }
        });
        RvNegocio.setAdapter(AdNeg);
        dbr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    if (dataSnapshot!=null) {
                        for (DataSnapshot Negocios : dataSnapshot.getChildren()) {
                            tokens.add(Negocios.child("tokens").getValue().toString());
                            Negocio neg = Negocios.getValue(Negocio.class);
                            Log.d("algo", "onDataChange: "+AlNegocios.size());
                            AlNegocios.add(neg);
                            Log.d("algo", "onDataChange: "+AlNegocios.size());
                            //tokens.add(Negocios.child("tokens").child("token").getValue().toString());
                            Log.d("algo", Negocios.child("Calificacion").exists()+"");
                            if (Negocios.child("Calificacion") != null) {
                                int E1 = Integer.parseInt(Negocios.child("Calificacion").child("1").getValue().toString());
                                int E2 = Integer.parseInt(Negocios.child("Calificacion").child("2").getValue().toString());
                                int E3 = Integer.parseInt(Negocios.child("Calificacion").child("3").getValue().toString());
                                int E4 = Integer.parseInt(Negocios.child("Calificacion").child("4").getValue().toString());
                                int E5 = Integer.parseInt(Negocios.child("Calificacion").child("5").getValue().toString());
                                int Suma = E1 + E2 + E3 + E4 + E5;
                                int SumaTodos = E1 + (E2 * 2) + (E3 * 3) + (E4 * 4) + (E5 * 5);
                                float estrellas;
                                if (Suma != 0) {
                                    estrellas = (float) (SumaTodos / Suma);
                                } else {
                                    estrellas = 3;
                                }
                                AlCalif.add(estrellas);
                                Log.d("algo", "promedio"+AlCalif.size());
                            }
                            else{
                                Log.d("algo", "3f");
                                AlCalif.add(3.0f);
                            }
                        }
                    } Log.d("algo", "nodos"+AlNegocios.size());
                        AdNeg.notifyDataSetChanged();
                }catch(Exception e){

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        return vista;
    }
}