package com.example.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.login.Adaptadores.AdaptadorNegocios;
import com.example.login.Adaptadores.AdaptadorPizzas;
import com.example.login.Modelos.Base;
import com.example.login.Modelos.Negocio;
import com.example.login.Modelos.PizzasPo;
import com.example.login.Modelos.tamaños;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class CardNegocios extends AppCompatActivity {
ImageView ImageNeg;
TextView NomNeg,DirNeg;
    ArrayList<Base> PizzasT;
    ArrayList <Float>AlCalif;
    ArrayList<String> tokens;
    RecyclerView RvNegocioPizzas;
    Context mContext;
    DatabaseReference dbr;

    AdaptadorPizzas adPi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext=getApplicationContext();
        setContentView(R.layout.activity_card_negocios);
        String Uid=getIntent().getStringExtra("UID").toString();
        String Imagen=getIntent().getStringExtra("Imagen").toString();
        final String Token=getIntent().getStringExtra("token").toString();
        Log.d("algo", Imagen);
        ImageNeg=findViewById(R.id.ImageNeg);
        NomNeg=findViewById(R.id.NomNeg);
        DirNeg=findViewById(R.id.DirNeg);

        Glide.with(mContext)
                .load(Imagen)
                .error(R.drawable.pizzafondo)
                .into(ImageNeg);
        NomNeg.setText(getIntent().getStringExtra("Nombre").toString());
        DirNeg.setText(getIntent().getStringExtra("Direccion").toString());

        PizzasT=new ArrayList<>();
        AlCalif=new ArrayList<>();
        tokens=new ArrayList<>();
        adPi=new AdaptadorPizzas(mContext,PizzasT);
        RvNegocioPizzas=findViewById(R.id.RvNegocioPizzas);
        RvNegocioPizzas.setLayoutManager(new GridLayoutManager(mContext,2));
        adPi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i=RvNegocioPizzas.getChildAdapterPosition(v);
                Intent intent=new Intent(mContext, CardPizzas.class);
                intent.putExtra("Nombre",PizzasT.get(i).getNombre());
                intent.putExtra("Imagen",PizzasT.get(i).getImagen_B());
                intent.putExtra("token",Token);
                intent.putExtra("Precio",PizzasT.get(i).getTamaños().get(0).getPrecio());
                ArrayList<String>nombres=new ArrayList<>();
                for (int j = 0; j < PizzasT.get(i).getTamaños().size(); j++) {
                    nombres.add(PizzasT.get(i).getTamaños().get(j).getNombre());
                }
                intent.putStringArrayListExtra("nombres",nombres);
                ArrayList<String>tamaños=new ArrayList<>();
                for (int j = 0; j < PizzasT.get(i).getTamaños().size(); j++) {
                    tamaños.add(PizzasT.get(i).getTamaños().get(j).getDiametro());
                }
                intent.putStringArrayListExtra("tamaños",tamaños);
                ArrayList<String>Precios=new ArrayList<>();
                for (int j = 0; j < PizzasT.get(i).getTamaños().size(); j++) {
                    Precios.add(PizzasT.get(i).getTamaños().get(j).getPrecio());
                }
                intent.putStringArrayListExtra("Precios",Precios);

                startActivity(intent);
            }
        });
        RvNegocioPizzas.setAdapter(adPi);
        dbr=FirebaseDatabase.getInstance().getReference().child("Pizzas").child(Uid);
        dbr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                PizzasT.clear();
                if (dataSnapshot!=null){
                    for (DataSnapshot Pizzas : dataSnapshot.getChildren()) {
                        Toast.makeText(mContext, "Pizzas"+Pizzas.getKey(), Toast.LENGTH_SHORT).show();
                        Base b=new Base();
                        b.setNombre(Pizzas.child("nombre").getValue().toString());
                        b.setImagen_B(Pizzas.child("Imagen_B").getValue().toString());
                        b.setPizzasPedidas(Pizzas.child("PizzasPedidas").getValue().toString());
                        b.setPizzaVistas(Pizzas.child("PizzaVistas").getValue().toString());
                        ArrayList<String> CIng=new ArrayList<>();
                        for (DataSnapshot Carnes:Pizzas.child("Carnes").getChildren()){
                            CIng.add(Carnes.getValue().toString());
                        }
                        b.setCarnes(CIng);
                        ArrayList<String> QIng=new ArrayList<>();
                        for (DataSnapshot Quesos:Pizzas.child("Quesos").getChildren()){
                            QIng.add(Quesos.getValue().toString());
                        }
                        b.setQuesos(QIng);
                        ArrayList<String> VIng=new ArrayList<>();
                        for (DataSnapshot Verduras:Pizzas.child("Frutas_Verduras_y_Hongos").getChildren()){
                            VIng.add(Verduras.getValue().toString());
                        }
                        b.setFrutas_Verduras_y_Hongos(VIng);
                        ArrayList<tamaños> TS=new ArrayList<>();
                        for (DataSnapshot Tam:Pizzas.child("Tamaños").getChildren()){
                            TS.add(Tam.getValue(tamaños.class));
                        }
                        b.setTamaños(TS);
                        PizzasT.add(b);
                    }
                    adPi.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


}
