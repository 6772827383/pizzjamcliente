package com.example.login.Modelos;

public class Pedidos {
    String Direccion;
    String Imagen_B;
    String NombreCliente;
    String Nombre_Negocio;
    String Numero_Cliente;
    String Precio_Pizza;
    String Tamaño_Pizza;
    String UidNegocio;
    String Uid_Pizza;
    String Hora;
    String nombrePizza;
    String Ingredientes;
    String token;
    String UidCliente;
    String UidPedido;
    String estado;
    String fecha_Pedido;

    public Pedidos() {

    }

    public Pedidos(String direccion, String imagen_B, String nombreCliente, String nombre_Negocio, String numero_Cliente, String precio_Pizza, String tamaño_Pizza, String uidNegocio, String uid_Pizza, String hora, String nombrePizza, String ingredientes, String token, String uidCliente, String uidPedido, String estado, String fecha_Pedido) {
        Direccion = direccion;
        Imagen_B = imagen_B;
        NombreCliente = nombreCliente;
        Nombre_Negocio = nombre_Negocio;
        Numero_Cliente = numero_Cliente;
        Precio_Pizza = precio_Pizza;
        Tamaño_Pizza = tamaño_Pizza;
        UidNegocio = uidNegocio;
        Uid_Pizza = uid_Pizza;
        Hora = hora;
        this.nombrePizza = nombrePizza;
        Ingredientes = ingredientes;
        this.token = token;
        UidCliente = uidCliente;
        UidPedido = uidPedido;
        this.estado = estado;
        this.fecha_Pedido = fecha_Pedido;
    }

    public String getFecha_Pedido() {
        return fecha_Pedido;
    }

    public void setFecha_Pedido(String fecha_Pedido) {
        this.fecha_Pedido = fecha_Pedido;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUidPedido() {
        return UidPedido;
    }

    public void setUidPedido(String uidPedido) {
        UidPedido = uidPedido;
    }

    public String getUidCliente() {
        return UidCliente;
    }

    public void setUidCliente(String uidCliente) {
        UidCliente = uidCliente;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getImagen_B() {
        return Imagen_B;
    }

    public void setImagen_B(String imagen_B) {
        Imagen_B = imagen_B;
    }

    public String getNombreCliente() {
        return NombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        NombreCliente = nombreCliente;
    }

    public String getNombre_Negocio() {
        return Nombre_Negocio;
    }

    public void setNombre_Negocio(String nombre_Negocio) {
        Nombre_Negocio = nombre_Negocio;
    }

    public String getNumero_Cliente() {
        return Numero_Cliente;
    }

    public void setNumero_Cliente(String numero_Cliente) {
        Numero_Cliente = numero_Cliente;
    }

    public String getPrecio_Pizza() {
        return Precio_Pizza;
    }

    public void setPrecio_Pizza(String precio_Pizza) {
        Precio_Pizza = precio_Pizza;
    }

    public String getTamaño_Pizza() {
        return Tamaño_Pizza;
    }

    public void setTamaño_Pizza(String tamaño_Pizza) {
        Tamaño_Pizza = tamaño_Pizza;
    }

    public String getUidNegocio() {
        return UidNegocio;
    }

    public void setUidNegocio(String uidNegocio) {
        UidNegocio = uidNegocio;
    }

    public String getUid_Pizza() {
        return Uid_Pizza;
    }

    public void setUid_Pizza(String uid_Pizza) {
        Uid_Pizza = uid_Pizza;
    }

    public String getHora() {
        return Hora;
    }

    public void setHora(String hora) {
        Hora = hora;
    }

    public String getNombrePizza() {
        return nombrePizza;
    }

    public void setNombrePizza(String nombrePizza) {
        this.nombrePizza = nombrePizza;
    }

    public String getIngredientes() {
        return Ingredientes;
    }

    public void setIngredientes(String ingredientes) {
        Ingredientes = ingredientes;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
