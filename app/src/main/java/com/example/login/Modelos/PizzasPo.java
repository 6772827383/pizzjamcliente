package com.example.login.Modelos;

public class PizzasPo {
    String nombre;
    String Imagen_B;
    String PizzaVistas;
    String PizzasPedidas;

    public PizzasPo() {
    }

    public PizzasPo(String nombre, String imagen_B, String pizzaVistas, String pizzasPedidas) {
        this.nombre = nombre;
        Imagen_B = imagen_B;
        PizzaVistas = pizzaVistas;
        PizzasPedidas = pizzasPedidas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImagen_B() {
        return Imagen_B;
    }

    public void setImagen_B(String imagen_B) {
        Imagen_B = imagen_B;
    }

    public String getPizzaVistas() {
        return PizzaVistas;
    }

    public void setPizzaVistas(String pizzaVistas) {
        PizzaVistas = pizzaVistas;
    }

    public String getPizzasPedidas() {
        return PizzasPedidas;
    }

    public void setPizzasPedidas(String pizzasPedidas) {
        PizzasPedidas = pizzasPedidas;
    }
}
