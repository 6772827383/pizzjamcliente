package com.example.login;

public class Item_tama {
   private String nombre;
    private String Precio;
    private String tamaño;
   private int imag;

    public Item_tama() {
    }

    public Item_tama(String nombre, String precio, String tamaño, int imag) {
        this.nombre = nombre;
        Precio = precio;
        this.tamaño = tamaño;
        this.imag = imag;
    }

    public String getPrecio() {
        return Precio;
    }

    public void setPrecio(String precio) {
        Precio = precio;
    }

    public String getTamaño() {
        return tamaño;
    }

    public void setTamaño(String tamaño) {
        this.tamaño = tamaño;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getImag() {
        return imag;
    }

    public void setImag(int imag) {
        this.imag = imag;
    }
}
